import rss_reader
import time


def init():
    while True:
        print('RssReader startup')
        reader = rss_reader.RssReader()
        reader.read()
        time.sleep(60)


if __name__ == '__main__':
    init()
