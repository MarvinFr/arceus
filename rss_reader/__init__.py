import re
import dateutil.parser
import feedparser
from database import MongoManager


class RssReader:

    def __init__(self):
        self.mongo_manager_source = MongoManager("arceus", "rssSource")
        self.mongo_manager_feed = MongoManager("arceus", "rssFeed")

    def cleanhtml(self, raw_html):
        cleanr = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
        return re.sub(cleanr, '', raw_html)

    def read(self):
        array_source = self.mongo_manager_source.get_all_documents()
        array_feed = self.mongo_manager_feed.get_all_documents()
        array_feed_link = []

        for document in array_feed:
            array_feed_link.append(document['link'])

        for source in array_source:
            news_feed = feedparser.parse(source['rssLink'])

            if source['testMode'] is True:
                continue

            print('Reading: ' + source['link'] + ', sik: ' + source['sik'])

            count = 0
            for entry in news_feed['entries']:
                # check if news item is already collected
                if self.mongo_manager_feed.get_document_by_key('link', str(entry['link'])) is None and \
                        self.mongo_manager_feed.get_document_by_key('title', str(entry['title'])):
                    # get image
                    imageLink = ''
                    try:
                        if source['hasImage']:
                            if eval(source['keys']['imageLink']) is None:
                                print('no image found')
                                continue
                            imageLink = eval(source['keys']['imageLink'])
                    except Exception as exception:
                        print('Can not find image')

                    clean_description = self.cleanhtml(entry.description)
                    pubDate = dateutil.parser.parse(entry.published)

                    insert_doc = {
                        'sik': source['sik'],
                        'link': entry.link,
                        'title': entry.title,
                        'description': clean_description,
                        'image': imageLink,
                        'pubDate': pubDate
                    }
                    self.mongo_manager_feed.insert_one_document(insert_doc)

                    print()
                    print(entry.link)
                    print(entry.title)
                    print(clean_description)
                    if imageLink is not None:
                        print(imageLink)
                    print(pubDate)
                    print()
                else:
                    continue
