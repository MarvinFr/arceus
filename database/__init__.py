from pymongo import MongoClient
import pymongo


class MongoManager:

    def __init__(self, database, collection):
        uri = "mongodb://localhost:27017/?readPreference=primary"
        self.mongoClient = MongoClient(uri)

        self.database = database
        self.collection = collection

    def insert_document_if_not_exists(self, document):
        self.mongoClient.get_database(self.database).get_collection(self.collection).update(document, document,
                                                                                            upsert=True)

    def insert_one_document(self, document):
        self.mongoClient.get_database(self.database).get_collection(self.collection).insert_one(document)

    def delete_document(self, document):
        self.mongoClient.get_database(self.database).get_collection(self.collection).delete_one(document)

    def document_exists(self, document):
        if self.mongoClient.get_database(self.database).get_collection(self.collection).find_one(document):
            return True
        return False

    def get_collection(self):
        return self.mongoClient.get_database(self.database).get_collection(self.collection)

    def get_collection_count(self):
        return self.mongoClient.get_database(self.database).get_collection(self.collection).count()

    def get_document(self, document):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find_one(document)
        return result

    def get_all_documents(self):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find()
        return result

    def get_document_by_key(self, key, value):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find_one({key: value})
        return result

    def get_sorted_documents(self):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find({}).sort('pubDate',
                                                                                                            pymongo.DESCENDING)
        return result

    def update_document(self, search_key, search_value, update_key, update_value):
        self.mongoClient.get_database(self.database).get_collection(self.collection).update_one(
            {search_key: search_value}, {"$set": {update_key: update_value}})

    def close_connection(self):
        self.mongoClient.close()
